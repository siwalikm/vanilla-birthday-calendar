## Vanilla Birthday Calendar App
----

Frontend is completely static and uses no libraries. The mock data is served via a node-express app server.

> For running app in local
```shell
git clone https://siwalikm@bitbucket.org/siwalikm/vanilla-birthday-calendar.git
cd vanilla-birthday-calendar
npm install
npm run start
```
After this, visit http://localhost:4000 in browser to run app.