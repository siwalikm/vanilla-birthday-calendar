var calendarStore = (function () {
  const ITEM_KEY = 'calendar-json';
  const API_HOST = ' https://demo1671003.mockable.io';

  function setCalendarData(data) {// year: json
    localStorage.setItem(ITEM_KEY, JSON.stringify(data, null, 4));
    return getCalendarData();
  }

  async function getCalendarData(resetFlag = false) {
    if (localStorage.getItem(ITEM_KEY) && !resetFlag) {
      return JSON.parse(localStorage.getItem(ITEM_KEY));
    } else {
      let response = await fetch(`${API_HOST}/birthdays`);
      let data = await response.json();
      setCalendarData(data);
      return JSON.parse(localStorage.getItem(ITEM_KEY));
    }
  }

  async function filterCalendarDataByYear(year) {// year: number
    let calData = await getCalendarData();
    return await calData.filter(personMeta => {
      return new Date(personMeta.birthday).getFullYear() === +year;
    });
  }

  function getAge(birthDate) {
    const yearInMs = 3.15576e+10 // Using a year of 365.25 days (because leap years)
    return (new Date() - new Date(birthDate).getTime()) / yearInMs;
  }

  function getDayFromDate(dateString) {
    const days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    const d = new Date(dateString);
    return days[d.getDay()];
  }

  function getInitialFromName(fullName) {
    const arrayFromName = fullName.split(' ');
    return arrayFromName.length > 1
      ? arrayFromName[0].charAt(0) + arrayFromName[arrayFromName.length - 1].charAt(0)
      : arrayFromName[0].charAt(0);
  }

  function transformData(calData) {
    // clones object and return clone with added new properties
    // const calData = getCalendarData();
    let calDataClone = JSON.parse(JSON.stringify(calData));
    return calDataClone.map(item => {
      item.initial = getInitialFromName(item.name);
      item.dayName = getDayFromDate(item.birthday);
      item.getAge = getAge(item.birthday);
      return item;
    });
  }

  return {
    getData(resetFlag) {
      return getCalendarData(resetFlag);
    },
    setData(JSON) {
      return setCalendarData(JSON);
    },
    filterByYear(year) {
      return filterCalendarDataByYear(year);
    },
    transformData(JSON) {
      return transformData(JSON);
    }
  }
})();





// On Page Load - fetching and populating data
document.addEventListener('DOMContentLoaded', async () => {
  let data = await calendarStore.getData();
  document.querySelector('.action-container__data-input').value
    = JSON.stringify(data, null, 4);
  document.querySelector('.action-container__timestamp--val').innerHTML
    = new Date().toLocaleString();
});

// On Textarea Data Update - Updating localstore data
document.querySelector('.action-container__data-input')
  .addEventListener('input', () => {
    document.querySelector('.action-container__timestamp--val')
      .innerHTML = new Date().toLocaleString();

    let updatedJsonFromInput
      = document.querySelector('.action-container__data-input').value;
    calendarStore.setData(JSON.parse(updatedJsonFromInput));
  });

// On Year Submit - Process Parse and Render calendar data using DOM fragment
document.querySelector('.action-container__data-update--submit')
  .addEventListener('click', async () => {
    let year = +document.querySelector('.action-container__data-update--input').value;
    if (year) {// skip process if year is empty
      let filteredData = await calendarStore.filterByYear(year);
      let transformedData = calendarStore.transformData(filteredData);

      const dayNames = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
      // Looping through each weekday and rendering it's UI
      dayNames.forEach(dayName => {
        let allEventsThisDay = transformedData.filter(calData => calData.dayName === dayName);

        // Sorting all events for a day by age of people
        if (allEventsThisDay.length > 1) {
          allEventsThisDay = allEventsThisDay.sort((a, b) => a.getAge - b.getAge);
        }

        // finding maximum item per row and column inside calendar grid
        var findGridValue = Math.ceil(Math.sqrt(allEventsThisDay.length));

        // Creating DOM Fragment and grid container for Weekday block
        var domFragment = document.createDocumentFragment();
        var container = document.createElement('div');
        container.className = 'day-block__details--grid';
        container.style.gridTemplateColumns = `repeat(${findGridValue}, 1fr)`;
        container.style.gridTemplateRows = `repeat(${findGridValue}, 1fr)`;
        if (!allEventsThisDay.length) {
          container.className = 'day--empty';
        } else {
          // Append each birthday block inside parent 'container'
          allEventsThisDay.forEach(event => {
            var row = document.createElement('div');
            row.className = 'day__person';
            row.append(document.createTextNode(event.initial));
            row.title = event.name;
            row.style.backgroundColor = "#xxxxxx".replace(/x/g, y => (Math.random() * 16 | 0).toString(16));
            container.append(row);
          });
        }
        // appending fragment tree to actual DOM
        domFragment.append(container);
        document.querySelector(`.day-block__details--${dayName}`).innerHTML = '';
        document.querySelector(`.day-block__details--${dayName}`).append(domFragment);
      });
    }
  });