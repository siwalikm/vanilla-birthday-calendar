let express = require('express');
let app = express();

let mockData = require('./data/stub-data.json');

app.use(express.static('public'));

app.listen(4000, function () {
  console.log('App running on http://localhost:4000')
})

app.get('/birthdays', function (req, res) {
  res.json(mockData);
});
